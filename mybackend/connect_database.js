const mongoose = require('mongoose')
const User = require('./model/User')

mongoose.connect('mongodb://dev:password@localhost:27017/mydb', { useNewUrlParser: true, useUnifiedTopology: true })

const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('connect')
})

User.find((err, users) => {
  if (err) return console.log(err)
  console.log(users)
})
