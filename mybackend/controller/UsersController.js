const User = require('../model/User')
const userController = {
  userList: [
    {
      id: 1,
      name: 'Surachet',
      gender: 'M'
    },
    {
      id: 2,
      name: 'Taylor',
      gender: 'M'
    }
  ],
  lastId: 3,

  async addUser(req, res, next) {
    const payload = req.body
    console.log(payload)
    const user = new User(payload)
    try {
      await user.save()
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateUser(req, res, next) {
    const payload = req.body
    // res.json(usersController.updateUser(payload))
    try {
      const user = await User.updateOne({ _id: payload._id }, payload)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteUser(req, res, next) {
    const { id } = req.params
    // res.json(usersController.deleteUser(id))
    try {
      const user = await User.deleteOne({ _id: id })
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUsers(req, res, next) {
    try {
      const users = await User.find({})
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUser(req, res, next) {
    const { id } = req.params
    try {
      const user = await User.findById(id)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = userController
